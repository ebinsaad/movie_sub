/**
 * Created by ebinsaad on 21/10/16.
 */

var express = require('express');
var fs  = require('fs');
var bodyParser = require('body-parser');

var app = express();

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));

app.set("view options", {
    layout: false
});

app.get('/', function (req, res) {
    res.render('public/index.html');
});

var port = process.env.port || 3000;

app.listen(port, function () {
    console.log("Express JS app running on port: "+ port);
});
